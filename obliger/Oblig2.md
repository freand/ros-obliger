# 1 og 2 
La inn $frontpage_limit = "500"; i /var/www/html/config.php i web1 og web2

# 3 
Lagde docker image. må ligge i rootmappen til bookface koden

```Docker
# Version 1.0
from ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
MAINTAINER carl aarrestad "carlarrestad@gmail.com"
RUN apt-get -y update
RUN apt-get -y install apache2 libapache2-mod-php php-pgsql
WORKDIR /var/www/html/
RUN rm -f index.html
COPY code .
COPY config.php .
EXPOSE 80
```

# 4 
bygg og kjør dockerfilen med:
```bash
sudo docker build -t apache .
sudo docker run -p 0.0.0.0:49155:80/tcp -P -d apache:latest /usr/sbin/apache2ctl -D FOREGROUND
sudo docker run -p 0.0.0.0:49156:80/tcp -P -d apache:latest /usr/sbin/apache2ctl -D FOREGROUND
```
```bash
ubuntu@dockeridoo:~/dockerimage/server1/bookface$ sudo docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED          STATUS          PORTS                   NAMES
43f2f8daf7fe   apache:latest   "/usr/sbin/apache2ct…"   25 minutes ago   Up 25 minutes   0.0.0.0:49155->80/tcp   hungry_aryabhata
a30eb0c8f5af   apache:latest   "/usr/sbin/apache2ct…"   25 minutes ago   Up 25 minutes   0.0.0.0:49156->80/tcp   flamboyant_wilbur
```

- endre /etc/haproxy/haproxy.cfg
```text
backend webservers
balance roundrobin
server www1 192.168.129.21
server www2 192.168.129.105
server docker1 192.168.132.105:49155
server docker2 192.168.132.105:49156
```


# 7 
docker ps -a viser alle docker instaner, også de som ikke kjører
 ```bash
ubuntu@dockeridoo:~/dockerimage/server1/bookface$ sudo docker ps -a
CONTAINER ID   IMAGE            COMMAND                  CREATED             STATUS                           PORTS                                     NAMES
43f2f8daf7fe   apache:latest    "/usr/sbin/apache2ct…"   32 minutes ago      Up 32 minutes                    0.0.0.0:49155->80/tcp                     hungry_aryabhata
a30eb0c8f5af   apache:latest    "/usr/sbin/apache2ct…"   32 minutes ago      Up 32 minutes                    0.0.0.0:49156->80/tcp                     flamboyant_wilbur
d89a43e85751   719be7d5f186     "/usr/sbin/apache2ct…"   About an hour ago   Exited (0) About an hour ago                                               objective_cori
f90f5bbb898a   56f6682fc858     "/bin/sh -c 'apt-get…"   About an hour ago   Exited (100) About an hour ago                                             boring_margulis
189f4e203f95   server1:latest   "-p 127.0.0.1:49156:…"   25 hours ago        Created                          0.0.0.0:49157->80/tcp, :::49157->80/tcp   modest_kapitsa
```
Du kan fjerne stoppede imager med:
```bash
sudo docker rm <docker_id|docker_navn>
```

Du kan gi docker imager navn med:
```bash
docker run -d -name detteblirnavnet carlaar:latest
```
# 10 
Hvis du rebooter vmen vil docker imagene ikke starte opp igjen, med mindre du gir de --restart\==always. slik:
```bash
sudo docker run --restart=always -p 0.0.0.0:49155:80  -P -d apache:latest /usr/sbin/apache2ctl -D FOREGROUND
```

# 12
Hva er docker swarm? Er det nyttig for oss?
> Docker swarm er en samling av instanser som får oppgaver av en leder (manager). Oppgavene fordeles automatisk utover instansene, selv hvis en av dem skulle gå ned. Docker swarm lar oss enkelt fordele instrastrukturen utover vmer og skalere ved behov. Det er svært robust, gjør at siden fortsetter å fungere selv om flere av vmene går ned. Det lar oss også oppdatere kode og instanser, uten å få et tjenestebrudd.

# 13
Scriptet nedenfor kan legges ved i "Configuration" når instansen lages. Dette sparer oss en del tid hvis vi skal sette opp en maskin flere ganger.
```bash
#!/bin/bash 
#Installs docker
sudo apt-get update
sudo apt-get -y install \
  ca-certificates \
  curl \
  gnupg \
  lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
```

# Uke 6
# 1
- Henter brukernavn og passord til servicebrukeren.
![picture 1](images/566c0635686cda7cc42f02ace74ba4ad4dca81eb4769a31eb4e7d6c243ea5132.png)  
- Laster ned rc filen etter å ha logget inn med servicebruker.
![picture 2](images/beb82238e1ca548a451bcf3f633990b80f44294cc5c64776da9394f13072992f.png)  

- Bruker scp for å sende rc fil til manager.
```
scp DCSG2003_V22_group28-openrc.sh ubuntu@manager:
```
- Legger til passord direkte i rc fil.
- Installerer openstack klient og sourcer rc fil.
```
sudo apt-get update
sudo apt-get install python3-openstackclient
source DCSG2003_V22_group15-openrc.sh
```
- For å slippe å source rc filen hver gang man går inn på manager må den legges i .bashrc (eller .zshrc)

# 2
```
cockroach --insecure --host=localhost sql --execute="use bf;select count(*) from users;"
cockroach --insecure --host=localhost sql --execute="use bf;select count(*) from posts;"
cockroach --insecure --host=localhost sql --execute="use bf;select count(*) from comments;"
```

# 4

```
# finnes.sh
#!/bin/bash
(ls $1 &> /dev/null && echo "$1 finnes") || echo "$1 finnes ikke"
```
# 7
```
openstack server remove floating ip "server-id" "floating ip-id" 
openstack floating ip create "float network"
```

# 8
- Oppskrift for å gå inn og ut av produksjon
```
# Start opp 

```
# Uke 7
# 1 Memcache som docker instans
```db
apt-get install -y php-memcache #Installerer memcache

# Installerer docker. Se dockerinstall i scripts.

docker run --name=memcache -p 11211:11211 -d memcached memcached -m 512 #Memcache container. Satt minne til 512mb

# Trenger et php bibliotek
apt-get install -y php-memcache

#Oppdaterer config.php til bf
$memcache_server = "ip-til-memchache-server";
$memcache_enabled = 1;
$memcache_enabled_pictures = 1;

- Kan bookface nå fungere uten memcache, altså dersom memcache serveren er av?
- Ja

- Hvor stor er ytelsesforbedringen på siden deres? Er ytelsesforbedringen stabil? ( man bør nok la det hele gå en stund før man vurderer dette )
- Det var en ytelsesforbedring på rundt 25%


- Kan dere se effekten på cockroachDB dashboard siden?
- Vi så at presset på databasen gikk ned

```
![picture 1](../images/d1f655636ebd8c29ab34a604b14a8f904e30e5803b8f6920e796557763279fea.png)  
Det funker :^)


# Uke 8
# 1
```
./newinstance.sh -n backup -f m1.tiny
scp .ssh/id_rsa backup:.ssh/id_rsa
```

# 3
Backupliste:
- vi tar bare backup av /bfdata

# 4
For å ta inkrementelle backups med cockroachdb trenger man en "enterprise license", men ser ut som det er ganske lett å gjøre (én kommando).

# 5
# 5.1
Hvordan rette opp dersom noen tar "delete * from posts" i databasen deres.

- skru av databasen
- scp forgje backup av /bf_data til db1
- overskriv nåværende /bf_data


# 5.2
Hvordan sette opp alt på nytt på en ny server dersom den opprinnelige db1 ville gått i stykker:
1. Lag ny VM med scriptet vårt, og få den til å kjøre db-install script
2. scp forgje /bf_data backup over til /bf_data åp nye db1
3. start database 


# 6 
Lag en liste over alt på manager som dere kunne tenkt å ta vare på: Config filer? SSH nøkler? (JA!) script? Dette kan lagres både som et git/gitlab repo eller sendes regelmessig til backup serveren. Lar det seg automatisere å samle disse filene mon tro?

- .ssh/config
- alt annet i .ssh
- alle andre configs har vi alt i git
- script ligger i git
- for å automatisere dette hadde jeg lagt en fil med pathen til filene og lagt et script som leser denne og tar backup av de

# Uke 9
# 1 
> Hva er hovedforskjellen mellom en databaseklynge ( databasecluster ) og en replikert database?

En databaseklynge har ikke nødvendigvis samme data på de forskjellige nodene, mens en replikert database alltid er identisk på de forskjellige nodene.


# 2 - Bookface Swarm
# Kapittel 1

``` # Server1, 2 og 3
# Synkroniser klokker
apt-get install -y ntpdate
ntpdate -b ntp.justervesenet.no

echo "/10 * * * * root ntpdate -b ntp.justervesenet.no" >> /etc/crontab

# Installer cockroachdb
wget https://binaries.cockroachdb.com/cockroach-v20.2.4.linux-amd64.tgz 
tar xzf cockroach-v20.2.4.linux-amd64.tgz 
cp cockroach-v20.2.4.linux-amd64/cockroach /usr/local/bin 
mkdir /bfdata

# Start cockroach
cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=server1:26257,server2:26257,server3:26257 --advertise-addr=serverX:26257 --max-offset=1500ms
```

``` # Server1
# Initialiser databasen
cockroach init --insecure --host=server1:26257
![picture 2](../images/368d33c00b0a2f51dd9279e4e5e76e57572fe084b9eff9663a77c3bc59f63e78.png)  

# Bookface.service
``` # /etc/systemd/system/bookface.service
 [Unit]
Description=Bookface script

[Service]
Type=simple
KillMode=process
ExecStart=/root/bookface_start.sh

[Install]
WantedBy=multi-user.target

# bookface_start.sh
#!/bin/bash

echo "Bookface auto start script"

cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=server1:26257,server2:26257,server3:26257 --advertise-addr=serverX:26257 --max-offset=1500ms

# Gjør scriptet kjørbart og skrur på service
chmod +x bookface_start.sh
systemctl enable bookface
grep -i bookface /var/log/syslog
...
Mar 18 13:18:03 server1 systemd[1]: bookface.service: Succeeded.

ubuntu@server1:~$ curl -s http://localhost:8080/_status/vars | grep livenodes
# HELP liveness_livenodes Number of live nodes in the cluster (will be 0 if this node is not itself live)
# TYPE liveness_livenodes gauge
liveness_livenodes 3

```

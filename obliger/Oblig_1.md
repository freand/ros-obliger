# 1 og 2
![[Pasted image 20220202105426.png]]
Lagde www1, www2 og balancer med gui. Balancer er allokert IPen 10.212.140.251.

# 3 
![[Pasted image 20220202105846.png]]Gjør 
Gjør det samme på www2.

# 4 - curl
![[Pasted image 20220202110050.png]]
PHP-koden ligger allerede på sidene, men her ser man at curl fungerer.

# 5 
![[Pasted image 20220202110600.png]]
```
# haproxy.cfg
frontend main
bind *:80
mode http
default_backend webservers

backend webservers
balance roundrobin
server www1 192.168.129.21
server www2 192.168.129.105
#server docker1 192.168.130.91:49155
#server docker2 192.168.130.91:49156


listen stats
bind *:1936
stats enable
stats uri /
stats hide-version
stats auth billybalancer:*somepassword*

```

```bash
ubuntu@balancer:~$ haproxy -c -f /etc/haproxy/haproxy.cfg
Configuration file is valid
```

```
sudo netstat -anltp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:1936            0.0.0.0:*               LISTEN      81784/haproxy       
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      81784/haproxy       
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      30684/systemd-resol 
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      30401/sshd: /usr/sb 
tcp        0      0 192.168.131.31:22       10.212.140.179:25902    ESTABLISHED 78738/sshd: ubuntu  
tcp        0      0 192.168.131.31:22       192.168.128.56:33232    ESTABLISHED 80914/sshd: ubuntu  
tcp        0    304 192.168.131.31:22       192.168.128.56:33258    ESTABLISHED 81800/sshd: ubuntu  
tcp        0      0 192.168.131.31:22       192.168.128.56:33230    ESTABLISHED 80745/sshd: ubuntu  
tcp        0      0 192.168.131.31:22       192.168.128.56:33244    ESTABLISHED 81464/sshd: ubuntu  
tcp6       0      0 :::22                   :::*                    LISTEN      30401/sshd: /usr/sb 
```
Vi ser at haproxy lytter på port 80 og 1936.

# 6 
Epost er sendt.


# 7 - Trelags-arkitektur

#### Hendelse 1 - Databasen går ned

Alle gruppene må i etterkant utforske hva som forårsakte at databasen gikk ned. Mens databasen er nede, må applikasjonsdrift sette nettsiden i lesemodus og informere brukere at ikke alle systemer er oppe. SAN og infrastruktur må informere brukere om at autentisering er nede.

#### [](#hendelse-2-oppgradering-av-server-fra-en-stabil-versjon-til-en-annen-eks-debian-10-til-debian-11)Hendelse 2 - Oppgradering av server fra en stabil versjon til en annen (eks Debian 10 til Debian 11)

Alle driftsgruppene blir nødt til å sjekke at tjenestene deres fungerer med den nye softwaren. Alle må være tilstede når oppgradering skjer for å kunne feilsøke hvis noe slutter å fungere.

#### [](#hendelse-3-gjennomgang-av-sikkerhetsrelatert-episode)Hendelse 3 - Gjennomgang av sikkerhetsrelatert episode

Alle må gå gjennom loggene sine for å se etter uvanlige hendelser. Brukere må informeres om mulig lekket informasjon. Alle driftsgruppene må undersøke systeme sine etter mulige sikkerhetshull.

#### [](#hendelse-4-skalering-av-systemer)Hendelse 4 - Skalering av systemer

Gruppene må vurdere hvilken del av deres system som er det svakeste leddet. Hvor det er behov for bedre utstyr.


# 8 - Database (uke 4)
Lagde db1

```bash
sudo apt install php-pgsql
```


satte opp db etter instruks
```bash
root@localhost:26257/bf> select * from users LIMIT 1;                                                                                                                         
        userid       |     name      |              picture               | status | posts | comments |        lastpostdate        |         createdate
---------------------+---------------+------------------------------------+--------+-------+----------+----------------------------+-----------------------------
 730843909648646145 | Yvonne Rivera | jEbgTQpgAIFXCWmDmcQaUgFIMrGGOk.jpg |        |    12 |        0 | 2022-01-30 17:26:08.680873 | 2022-01-25 10:24:21.074102
  
  ```
  
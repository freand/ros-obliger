# Uke 10

Her er vår bookface_start.sh som sjekker om glusterfs volumene er mountet, så starter vi cockroach.

```bash
ubuntu@server1:~$ cat bookface_start.sh 
#!/bin/bash

ipServer1="192.168.132.197"
ipServer2="192.168.133.156"
ipServer3="192.168.133.250"

echo "Bookface auto start script"

current=""

if [[ $(hostname) == server1 ]];then
    current=$ipServer1
elif [[ $(hostname) == server2 ]]; then
    current=$ipServer2
else [[ $(hostname) == server3 ]];
    current=$ipServer3
fi

mount -t glusterfs current:bf_config /bf_config
sleep 5
while [ ! -f /bf_config/jeg_er_mountet ]; do
sleep 5
mount -t glusterfs $current:bf_config /bf_config
done
mount -t glusterfs $current:bf_images /bf_images


cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$ipServer1:26257,$ipServer2:26257,$ipServer3:26257 --advertise-addr=$current:26257 --max-offset=1500ms
if [ $? -gt 0 ]; then
    echo 1
    exit
fi
systemctl start docker
echo 0
```

Her ser man at alle volumene er oppe: 
```bash
ubuntu@server1:~$ sudo gluster volume status
Status of volume: bf_config
Gluster process                             TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick 192.168.132.197:/config_brick         49152     0          Y       1017 
Brick 192.168.133.156:/config_brick         49152     0          Y       931  
Brick 192.168.133.250:/config_brick         49152     0          Y       920  
Self-heal Daemon on localhost               N/A       N/A        Y       1039 
Self-heal Daemon on 192.168.133.156         N/A       N/A        Y       954  
Self-heal Daemon on 192.168.133.250         N/A       N/A        Y       945  
 
Task Status of Volume bf_config
------------------------------------------------------------------------------
There are no active volume tasks
 
Status of volume: bf_images
Gluster process                    `        TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick 192.168.132.197:/bf_brick             49153     0          Y       1028 
Brick 192.168.133.156:/bf_brick             49153     0          Y       942  
Brick 192.168.133.250:/bf_brick             49153     0          Y       934  
Self-heal Daemon on localhost               N/A       N/A        Y       1039 
Self-heal Daemon on 192.168.133.156         N/A       N/A        Y       954  
Self-heal Daemon on 192.168.133.250         N/A       N/A        Y       945  
 
Task Status of Volume bf_images
------------------------------------------------------------------------------
There are no active volume tasks
```


# Uke 11

Her ser vi at alle docktjenestene er oppe og kjøre 
```bash
ubuntu@server1:~$ sudo docker service ls
ID             NAME                   MODE         REPLICAS   IMAGE                                          PORTS
wfn1c2fpis65   bf_db_balance          replicated   1/1        192.168.128.5:5000/haproxy:latest              *:1936->1936/tcp
nfxvmcpmznf3   bf_memcache            replicated   1/1        192.168.128.5:5000/memcached:latest            
qqae9tnnbq91   bf_web                 replicated   3/3        192.168.128.5:5000/bf:latest                   *:80->80/tcp
312q68fdoe50   mon_alertmanager       replicated   1/1        stefanprodan/swarmprom-alertmanager:v0.14.0    
wut4bihy4cfn   mon_caddy              replicated   1/1        stefanprodan/caddy:latest                      *:3000->3000/tcp, *:9090->9090/tcp, *:9093-9094->9093-9094/tcp
3kqux5b59uyz   mon_cadvisor           global       3/3        google/cadvisor:latest                         
yicdsw310n5y   mon_dockerd-exporter   global       3/3        stefanprodan/caddy:latest                      
39w49gzogufg   mon_grafana            replicated   1/1        stefanprodan/swarmprom-grafana:5.3.4           
ncxq370cx9w1   mon_node-exporter      global       3/3        stefanprodan/swarmprom-node-exporter:v0.16.0   
nmsv2z1f24p9   mon_prometheus         replicated   1/1        stefanprodan/swarmprom-prometheus:v2.5.0       
jtfg1vai0oxt   mon_unsee              replicated   1/1        cloudflare/unsee:v0.8.0
```

# Uke 12
## Oppgave 1
2
1. 
Tallene viser at selv om spillet kan nå nesten femti tusen spillere på det meste, så er det vanligst at spillertallet er rundt tjue tusen. Gjennomsnittet ligger på 15000 spillere.

Hvis vi ser på de øverste 5 prosentene ser vi at sannsyneligheten for at det er så mange brukere er under 0.1 prosent. Dette stemmer bra overenst med at variancen på datasettet er veldig høyt. 

2.
Det er generellt flere brukere i 3k_end enn i 3k_start. Det nye datasettet har høyere variance, og man ser at maks antall spillere og 95% maks har steget mer en medianen. Dette betyr at det har vært noen perioder med store avvik med flere spillere i det nye settet.

3.
Vi kan utifra datasettet konkludere at antallet spillere har økt fra målingene startet og til de sluttet. Det gjennomsnittlige antallet spillere har gått fra 13140 til 18823, en økning på nesten 6000 spillere. Samtidig har forskjellen mellom maks antall spillere og min antall spillere gått opp. Det nye dataen har spillertopper som har økt med rundt 9000, mens spillerbunnene bare har økt med 2500 spillere. Dette hjelper med å dra gjennomsnittet opp, men gjennomsnittet blir lenger unna forventet spillerantall.

## oppgave 2

```bash
ubuntu@manager ~$ curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="BillyBaba"}'                                                
{"status":"success","data":{"resultType":"vector","result":[{"metric":{"__name__":"last_download_time","instance":"192.168.128.5:9001","job":"mongodb","name":"BillyBaba","tags":"dcsg2003_22"},"value":[1649252369.875,"48.133961916"]}]}}
```
frontpage_scale.sh sjekker lastetiden, hvis den er under en viss tid økes antall brukere som vises på forsiden, hvis den er over senkes antall brukere. Vi kan bruke det for å automatisk skalere forsiden slik at vi ligger nærmest mulig nedlastingstidsgrensen for å optimalisere inntjening av kyrrecoins.|

## oppgave 3


Nettsiden kommer under 11 sekunder lastetid i 90 % av tilfellene, en treg lastetid i forhold til bonusen som ligger på rundt 6 sekunder. Dette er ikke overraskende ettersom vi har hatt mye nedetid på tjenesten i det siste. Vi har slitt med synkronisering av serverklokkene, som har ført til at databasen har vært nede.
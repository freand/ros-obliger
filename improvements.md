# Database
- Connect two more instances for a proper cluster. Current memory and capacity usage is very low. 
    - 3x m1.tiny kanskje (2gb ram hver, 0.02 coins)

# Prizes
```

Flavor 	Price
m1.micro 	0.01
m1.tiny 	0.02
m1.small 	0.04
m1.medium 	0.08
m1.large 	0.16
m1.xlarge 	0.32
m1.2xlarge 	0.64
t1.small 	0.01
t1.medium 	0.015
t1.tiny 	0.005
t1.large 	0.04
t1.xlarge 	0.06
c1.tiny 	0.20
```
#### wwww1 og wwww2

```
apt-get update
apt-get install apache2 libapache2-mod-php
rm /var/www/html/index.html
vim /var/www/html/index.html
```

#### balancer
Knyttet til floating IP

```
apt-get update

apt-get install haproxy net-tools
vim /etc/haproxy/haproxy.cfg

" //Legg til dette
frontend main
bind *:80
mode http
default_backend webservers

backend webservers
balance roundrobin
server www1 IP TIL WWW1 (192.168.129.21)
server www2 IP TIL WWW2 (192.168.129.105)

listen stats
bind *:1936
stats enable
stats uri /
stats hide-version
stats auth billybalancer:nyttpassword 
"

haproxy -c -f /etc/haproxy/haproxy.cfg //Sjekk at det funker
service haproxy start

netstat -anltp //Sjekk at haproxy er oppe. Restart systemctl hvis det ikke funker.
```
Åpne port 80 i security groups

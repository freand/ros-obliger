#!/bin/bash
ip1="192.168.129.21"
ip2="192.168.129.105"

echo "########################################################"
figlet -t -k -f /usr/share/figlet/small.flf "BillyBaba®"
echo -en "######################################################## \n\n" 

echo "Collecting data, may take some time"

echo -ne '#####                     (33%)\r'

accessCountStable=$(ssh $ip1 "wc -l /var/log/apache2/access.log" | awk '{print $1}')
errorCountStable=$(ssh $ip1 "wc -l /var/log/apache2/error.log" | awk '{print $1}')

echo -ne '#############             (66%)\r'

accessCountUnstable=$(ssh $ip2 "wc -l /var/log/apache2/access.log" | awk '{print $1}')
errorCountUnstable=$(ssh $ip2 "wc -l /var/log/apache2/error.log" | awk '{print $1}')

echo -ne '#######################   (100%)\r'
sleep 0.2
echo "                                          "
errorpercent=$(bc <<< "scale=2; ${errorCountStable}*100/${accessCountStable}")
echo -e "\n\e[4mStable\e[0m"
echo "Stable access count: $accessCountStable"
echo "Stable error count:  $errorCountStable"
echo "Stable percent error: ${errorpercent}%"

errorpercent=$(bc <<< "scale=2; ${errorCountUnstable}*100/${accessCountUnstable}")
echo -e "\n\e[4mUnstable\e[0m"
echo "Unstable access count: $accessCountUnstable"
echo "Unstable error count:  $errorCountUnstable"
echo "Unstable percent error: ${errorpercent}%"

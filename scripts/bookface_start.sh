#!/bin/bash

ipServer1="192.168.132.197"
ipServer2="192.168.133.156"
ipServer3="192.168.133.250"

echo "Bookface auto start script"

current=""

if [[ $(hostname) == server1 ]];then
    current=$ipServer1
elif [[ $(hostname) == server2 ]]; then
    current=$ipServer2
else [[ $(hostname) == server3 ]];
    current=$ipServer3
fi

mount -t glusterfs current:bf_config /bf_config
sleep 5
while [ ! -f /bf_config/jeg_er_mountet ]; do
sleep 5
mount -t glusterfs current:bf_config /bf_config
done
mount -t glusterfs current:bf_images /bf_images


cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$ipServer1:26257,$ipServer2:26257, $ipServer3:26257 --advertise-addr=$current:26257 --max-offset=1500ms
if [ $? -gt 0 ]; then
    echo failed
    exit
fi
echo succes

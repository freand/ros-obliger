#!/bin/bash

# Create instance. 
# -n [name]
# -f [flavor]
# -s [script file]

TEMP=$(getopt -o n:f:s: -l name:,flavor:,script: -n "instancer" -- "$@")

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

eval set -- "$TEMP" 


IMAGE_ID=2dcc56bf-6f1e-47aa-8b6d-4096009d9c30 #Focal Fossa aka Ubuntu Server 20.04 LTS
KEY_NAME=Manager-Group28
SEC_GROUP_NAME=default
FLAVOR_ID="m1.tiny"
USER_DATA_FILE=
INSTANCE_NAME=
IP="|" #Makes sense, trust me.
while true; do
	echo "$@";
    case "$1" in
        -n | --name ) INSTANCE_NAME="$2"; shift 2;;
        -f | --flavor ) FLAVOR_ID="$2"; shift 2;;
        -s | --script ) USER_DATA_FILE="$2"; shift 2;;
        -- ) shift; break;;
        * )  break;;
    esac
done

if [ ${#USER_DATA_FILE} -gt 0 ]; then
  openstack server create --flavor $FLAVOR_ID --image $IMAGE_ID --key-name $KEY_NAME --user-data $USER_DATA_FILE --security-group $SEC_GROUP_NAME --property KEY=VALUE $INSTANCE_NAME; else
  openstack server create --flavor $FLAVOR_ID --image $IMAGE_ID --key-name $KEY_NAME --security-group $SEC_GROUP_NAME --property KEY=VALUE $INSTANCE_NAME;
fi

if [ $? != 0 ]; then
  exit 1; 
elif [ IP == "|" ]; then
  sleep 2;
  IP=$(openstack server list --name $INSTANCE_NAME | awk -v hostname="$INSTANCE_NAME" '$0 ~ hostname { print $8 }' | cut -f2- -d=);
fi
 
echo -e "Host $INSTANCE_NAME \n Hostname $IP \n User ubuntu \n IdentityFile /home/ubuntu/.ssh/MgrG28.pem" >> /home/ubuntu/.ssh/config


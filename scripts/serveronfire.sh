#!/bin/bash

# TODO: get hosts from /etc/hosts using awk magic.

declare -A hosts

hosts[db1]="192.168.132.21"
hosts[balancer]="192.168.131.31"
hosts[www1]="192.168.129.21"
hosts[www2]="192.168.129.105"

echo "Checking the following hosts:"
for host in "${!hosts[@]}";do 
    echo $host: ${hosts[${host}]}; 
done;
echo -e "\n";

for host in "${!hosts[@]}"; do
    echo "Checking $host:"
    nc -z $host 22;
    if [ $? != 0 ]; 
    then
        echo -e "$host is probably down...\n";
	status=$(nova list | awk -v hostname="$host" '$0 ~ hostname { print $6 }'); #bruker -v for � sette variabel inn i awk. $0 ~ hostname er filteret. 
        echo "Openstack status is: $status";
	case "$status" in
		"SHUTOFF" ) echo "Starting..."; nova start $host;;
		"ACTIVE" ) echo "Rebooting..."; nova reboot $host;;
		"SUSPENDED" ) echo "Unsuspending... "; nova resume $host;;
		"PAUSED" ) echo "Unpausing..."; nova unpause $host;;
		* ) echo "Unknown state, doing nothing..."; break;;
	esac
    else 
        echo -e "$host is probably up!\n";
    fi  
done;

#!/bin/bash
# Installs ntpdate and cockroachdb. Next step is to initialize cluster.
if [ "$EUID" -ne 0 ]
	then echo "Are you root?"
	exit
fi

apt-get install -y ntpdate
ntpdate -b ntp.justervesenet.no

echo "*/10 * * * * root ntpdate -b ntp.justervesenet.no" >> /etc/cron.d/ntp

wget https://binaries.cockroachdb.com/cockroach-v20.2.4.linux-amd64.tgz 
tar xzf cockroach-v20.2.4.linux-amd64.tgz 
cp cockroach-v20.2.4.linux-amd64/cockroach /usr/local/bin 
mkdir /bfdata

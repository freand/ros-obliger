# Database
> To access the internal cockroachdb dashboard you need a proxy into the network.
1. Setup [FoxyProxy](https://web.microsoftstream.com/video/996de8cd-73df-4cc5-a3e6-1cd10a24e880)
2. Reverse proxy to manager 

```ssh -D 3000 ubuntu@10.212.140.179 -i ~/.ssh/ros/ros```

3. Enable FoxyProxy
4. Go to the database ip and port 8080 in your browser. (http://192.168.132.21:8080)
User:"bfuser"
Secret key: "hemmelig" også i grafana!
